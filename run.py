import os
from selenium import webdriver
from time import sleep

driver = webdriver.Chrome(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'chromedriver'))
driver.get('http://ebay.com/')
driver.set_window_size(1280, 5000)
driver.save_screenshot('screen.png')
sleep(10)
driver.close()
driver.quit()
